from preprocessing import *
import numpy as np
from keras.models import Sequential
from sklearn.metrics import mean_squared_error
from keras.callbacks import EarlyStopping
from keras.layers import LSTM, Dense
import math
from sklearn.preprocessing import MinMaxScaler


def generate_sequences(df, seq_length, column):
    predict_values = df.loc[:, df.columns == column].values
    df_values = df.values
    X, y = list(), list()
    for i in range(len(df_values)):
        end_ix = i + seq_length
        if end_ix > len(df_values) - 1:
            break
        seq_x, seq_y = df_values[i:end_ix, :-1], predict_values[end_ix]
        X.append(seq_x)
        y.append(seq_y)
    return np.array(X), np.array(y)


def train_test_split(df, train_percentage: float):
    train_size = int(len(df) * train_percentage)
    train, test = df[:train_size], df[train_size:]
    return train, test


def lstm_model(x):
    num_sequences = x.shape[1]
    num_features = x.shape[2]
    model = Sequential()
    model.add(LSTM(4, input_shape=(num_sequences, num_features)))
    model.add(Dense(num_features))
    model.add(Dense(1))
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model


def fit_model(model, X_train, y_train, epochs, batch_size, val_split, plot=True):
    # early stopping doesnt work for this model for some reason
    callback = EarlyStopping(monitor='loss', patience=3)
    history = model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size,
                        validation_split=val_split, shuffle=False, callbacks=[callback]).history
    return model


if __name__ == "__main__":
    RESULT_DICT = {
        'NO2 all columns': {
            'RMSE': 0,
            'MAE': 0
        },
        'PM25 all columns': {
            'RMSE': 0,
            'MAE': 0
        },
        'NO2 removed seasons': {
            'RMSE': 0,
            'MAE': 0
        },
        'PM25 removed seasons': {
            'RMSE': 0,
            'MAE': 0
        },
        'NO2 removed other columns': {
            'RMSE': 0,
            'MAE': 0
        },
        'PM25 removed other columns': {
            'RMSE': 0,
            'MAE': 0
        },
    }
    original_df = pd.read_csv(DATA_PATH)
    original_df = convert_types(original_df)
    original_df = process_missing_values(original_df)
    original_df['season'] = original_df['date'].apply(get_season)
    original_df = original_df.sort_values(by="date")
    original_df = one_hot_encoding(original_df, ONE_HOT_ENCODING_COLUMN_NAMES)
    original_df = original_df.drop(columns=['date'])
    original_df = original_df.drop(columns=[' so2'])
    columns_to_predict = ' no2'

    df = original_df.copy()

    results = original_df.loc[:, original_df.columns == columns_to_predict]
    column_names = df.columns
    df = df.values
    scaler = MinMaxScaler()
    result_scaler = MinMaxScaler()
    result_scaler.fit(results)
    df = scaler.fit_transform(df)
    df = pd.DataFrame(df, columns=column_names)

    train, test = train_test_split(df, .8)

    window_len = 5
    X_train, y_train = generate_sequences(train, seq_length=window_len, column=columns_to_predict)
    X_test, y_test = generate_sequences(test, seq_length=window_len, column=columns_to_predict)
    model = lstm_model(X_train)
    model = fit_model(model, X_train, y_train, epochs=100, batch_size=32, val_split=0.05)
    trainPredict = model.predict([[X_train]])
    testPredict = model.predict([[X_test]])
    trainPredict = result_scaler.inverse_transform(trainPredict)
    y_train = result_scaler.inverse_transform(y_train)
    testPredict = result_scaler.inverse_transform(testPredict)
    y_test = result_scaler.inverse_transform(y_test)

    testScore = math.sqrt(mean_squared_error(y_test, testPredict[:, 0]))
    RESULT_DICT['NO2 all columns']['RMSE'] = testScore
    testScore = mean_squared_error(y_test, testPredict[:, 0])
    RESULT_DICT['NO2 all columns']['MAE'] = testScore

    df = original_df.copy()
    df = df.drop(columns=['season_autumn', 'season_spring', 'season_summer', 'season_winter'])
    results = original_df.loc[:, original_df.columns == columns_to_predict]
    column_names = df.columns
    df = df.values
    scaler = MinMaxScaler()
    result_scaler = MinMaxScaler()
    result_scaler.fit(results)
    df = scaler.fit_transform(df)
    df = pd.DataFrame(df, columns=column_names)
    train, test = train_test_split(df, .8)
    window_len = 5
    X_train, y_train = generate_sequences(train, seq_length=window_len, column=columns_to_predict)
    X_test, y_test = generate_sequences(test, seq_length=window_len, column=columns_to_predict)
    model = lstm_model(X_train)
    model = fit_model(model, X_train, y_train, epochs=100, batch_size=32, val_split=0.05)

    testPredict = model.predict([[X_test]])

    trainPredict = result_scaler.inverse_transform(trainPredict)
    y_train = result_scaler.inverse_transform(y_train)
    testPredict = result_scaler.inverse_transform(testPredict)
    y_test = result_scaler.inverse_transform(y_test)

    testScore = math.sqrt(mean_squared_error(y_test, testPredict[:, 0]))
    RESULT_DICT['NO2 removed seasons']['RMSE'] = testScore
    testScore = mean_squared_error(y_test, testPredict[:, 0])
    RESULT_DICT['NO2 removed seasons']['MAE'] = testScore

    df = original_df.copy()
    df = df.drop(columns=[' pm10', ' o3', ' pm25', ' co'])
    results = original_df.loc[:, original_df.columns == columns_to_predict]
    column_names = df.columns
    df = df.values
    scaler = MinMaxScaler()
    result_scaler = MinMaxScaler()
    result_scaler.fit(results)
    df = scaler.fit_transform(df)
    df = pd.DataFrame(df, columns=column_names)
    train, test = train_test_split(df, .8)
    window_len = 5
    X_train, y_train = generate_sequences(train, seq_length=window_len, column=columns_to_predict)
    X_test, y_test = generate_sequences(test, seq_length=window_len, column=columns_to_predict)
    model = lstm_model(X_train)
    model = fit_model(model, X_train, y_train, epochs=100, batch_size=32, val_split=0.05)

    trainPredict = model.predict([[X_train]])
    testPredict = model.predict([[X_test]])

    trainPredict = result_scaler.inverse_transform(trainPredict)
    y_train = result_scaler.inverse_transform(y_train)
    testPredict = result_scaler.inverse_transform(testPredict)
    y_test = result_scaler.inverse_transform(y_test)
    testScore = math.sqrt(mean_squared_error(y_test, testPredict[:, 0]))
    RESULT_DICT['NO2 removed other columns']['RMSE'] = testScore
    testScore = mean_squared_error(y_test, testPredict[:, 0])
    RESULT_DICT['NO2 removed other columns']['MAE'] = testScore

    original_df = pd.read_csv(DATA_PATH)
    original_df = convert_types(original_df)
    original_df = process_missing_values(original_df)
    original_df['season'] = original_df['date'].apply(get_season)
    original_df = original_df.sort_values(by="date")
    original_df = one_hot_encoding(original_df, ONE_HOT_ENCODING_COLUMN_NAMES)
    original_df = original_df.drop(columns=['date'])
    original_df = original_df.drop(columns=[' so2'])
    columns_to_predict = ' pm25'
    df = original_df.copy()
    results = original_df.loc[:, original_df.columns == columns_to_predict]
    column_names = df.columns
    df = df.values
    scaler = MinMaxScaler()
    result_scaler = MinMaxScaler()
    result_scaler.fit(results)
    df = scaler.fit_transform(df)
    df = pd.DataFrame(df, columns=column_names)
    train, test = train_test_split(df, .8)
    window_len = 5
    X_train, y_train = generate_sequences(train, seq_length=window_len, column=columns_to_predict)
    X_test, y_test = generate_sequences(test, seq_length=window_len, column=columns_to_predict)
    model = lstm_model(X_train)
    model = fit_model(model, X_train, y_train, epochs=100, batch_size=32, val_split=0.05)
    testPredict = model.predict([[X_test]])
    trainPredict = result_scaler.inverse_transform(trainPredict)
    y_train = result_scaler.inverse_transform(y_train)
    testPredict = result_scaler.inverse_transform(testPredict)
    y_test = result_scaler.inverse_transform(y_test)
    testScore = math.sqrt(mean_squared_error(y_test, testPredict[:, 0]))
    RESULT_DICT['PM25 all columns']['RMSE'] = testScore
    testScore = mean_squared_error(y_test, testPredict[:, 0])
    RESULT_DICT['PM25 all columns']['MAE'] = testScore


    df = original_df.copy()
    df = df.drop(columns=['season_autumn', 'season_spring', 'season_summer', 'season_winter'])

    results = original_df.loc[:, original_df.columns == columns_to_predict]
    column_names = df.columns
    df = df.values
    scaler = MinMaxScaler()
    result_scaler = MinMaxScaler()
    result_scaler.fit(results)
    df = scaler.fit_transform(df)
    df = pd.DataFrame(df, columns=column_names)

    train, test = train_test_split(df, .8)

    window_len = 5
    X_train, y_train = generate_sequences(train, seq_length=window_len, column=columns_to_predict)
    X_test, y_test = generate_sequences(test, seq_length=window_len, column=columns_to_predict)
    model = lstm_model(X_train)
    model = fit_model(model, X_train, y_train, epochs=100, batch_size=32, val_split=0.05)

    trainPredict = model.predict([[X_train]])
    testPredict = model.predict([[X_test]])

    trainPredict = result_scaler.inverse_transform(trainPredict)
    y_train = result_scaler.inverse_transform(y_train)
    testPredict = result_scaler.inverse_transform(testPredict)
    y_test = result_scaler.inverse_transform(y_test)
    testScore = math.sqrt(mean_squared_error(y_test, testPredict[:, 0]))
    RESULT_DICT['PM25 removed seasons']['RMSE'] = testScore
    testScore = mean_squared_error(y_test, testPredict[:, 0])
    RESULT_DICT['PM25 removed seasons']['MAE'] = testScore


    df = original_df.copy()
    df = df.drop(columns=[' pm10', ' o3', ' no2', ' co'])

    results = original_df.loc[:, original_df.columns == columns_to_predict]
    column_names = df.columns
    df = df.values
    scaler = MinMaxScaler()
    result_scaler = MinMaxScaler()
    result_scaler.fit(results)
    df = scaler.fit_transform(df)
    df = pd.DataFrame(df, columns=column_names)

    train, test = train_test_split(df, .8)

    window_len = 5
    X_train, y_train = generate_sequences(train, seq_length=window_len, column=columns_to_predict)
    X_test, y_test = generate_sequences(test, seq_length=window_len, column=columns_to_predict)
    model = lstm_model(X_train)
    model = fit_model(model, X_train, y_train, epochs=100, batch_size=32, val_split=0.05)

    trainPredict = model.predict([[X_train]])
    testPredict = model.predict([[X_test]])

    trainPredict = result_scaler.inverse_transform(trainPredict)
    y_train = result_scaler.inverse_transform(y_train)
    testPredict = result_scaler.inverse_transform(testPredict)
    y_test = result_scaler.inverse_transform(y_test)

    testScore = math.sqrt(mean_squared_error(y_test, testPredict[:, 0]))
    RESULT_DICT['PM25 removed other columns']['RMSE'] = testScore
    testScore = mean_squared_error(y_test, testPredict[:, 0])
    RESULT_DICT['PM25 removed other columns']['MAE'] = testScore

    print(RESULT_DICT)
