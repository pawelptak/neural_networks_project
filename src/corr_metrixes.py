import seaborn as sn
import matplotlib.pyplot as plt
from preprocessing import *


if __name__ == '__main__':
    original_df = pd.read_csv(DATA_PATH)
    original_df = convert_types(original_df)
    original_df = process_missing_values(original_df)
    df = original_df.drop(['date'], axis=1)

    corrMatrix = df.corr().abs()
    sn.heatmap(corrMatrix)
    plt.title('Correlation Heatmap Wroclaw Wisniowa')
    plt.show()

    path = 'raw_data/beijing-air-quality.csv'
    original_df = pd.read_csv(path)
    original_df = convert_types(original_df)
    original_df[' pm10'] = pd.to_numeric(original_df[' pm10'])
    original_df[' o3'] = pd.to_numeric(original_df[' o3'])
    original_df[' so2'] = pd.to_numeric(original_df[' so2'])
    original_df = process_missing_values(original_df)
    df = original_df.drop(['date'], axis=1)

    corrMatrix = df.corr().abs()
    sn.heatmap(corrMatrix)
    plt.title('Correlation Heatmap Beijing')
    plt.show()
