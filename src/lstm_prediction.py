from preprocessing import *
import numpy as np
import tensorflow
import numpy
import matplotlib.pyplot as plt
from keras.models import Sequential
from sklearn.metrics import mean_squared_error, mean_absolute_error
from keras.callbacks import EarlyStopping
from keras.layers import LSTM, Dense
from keras.layers import Bidirectional
from keras.models import load_model
import math
from sklearn.preprocessing import MinMaxScaler

# main source: https://machinelearningmastery.com/time-series-prediction-lstm-recurrent-neural-networks-python-keras/


def generate_sequences(df, seq_length, column):
    predict_values = df.loc[:, df.columns == column].values
    df_values = df.values
    X, y = list(), list()
    for i in range(len(df_values)):
        end_ix = i + seq_length
        if end_ix > len(df_values) - 1:
            break
        # gather input and output parts of the pattern
        seq_x, seq_y = df_values[i:end_ix, :-1], predict_values[end_ix]
        X.append(seq_x)
        y.append(seq_y)
    return np.array(X), np.array(y)


def train_test_split(df, train_percentage: float):
    train_size = int(len(df) * train_percentage)
    train, test = df[:train_size], df[train_size:]
    # print(f"Training data: {len(train)}, test data {len(test)}")
    return train, test


def lstm_model(x):
    num_sequences = x.shape[1]
    num_features = x.shape[2]
    model = Sequential()
    model.add(LSTM(4, input_shape=(num_sequences, num_features)))
    model.add(Dense(num_features))
    model.add(Dense(1))
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model


def lstm_bidirectional_model(x):
    num_sequences = x.shape[1]
    num_features = x.shape[2]
    model = Sequential()
    model.add(Bidirectional(LSTM(units=50, activation='relu', input_shape=(num_sequences, num_features))))
    model.add(Dense(num_features))
    model.add(Dense(1))
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model


def fit_model(model, X_train, y_train, epochs, batch_size, val_split, plot=True):
    # early stopping doesnt work for this model for some reason
    callback = EarlyStopping(monitor='loss', patience=3)
    history = model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size,
                        validation_split=val_split, shuffle=False, callbacks=[callback], verbose=0).history

    if plot:
        # plot the training losses
        fig, ax = plt.subplots(figsize=(14, 6), dpi=80)
        ax.plot(history['loss'], 'b', label='Train', linewidth=2)
        ax.plot(history['val_loss'], 'r', label='Validation', linewidth=2)
        ax.set_title('Model loss', fontsize=16)
        ax.set_ylabel('Loss (mae)')
        ax.set_xlabel('Epoch')
        ax.legend(loc='upper right')
        plt.show()

    model.save('lstm_model.h5')

def meaner(list):
    return sum(list) / len(list)

if __name__ == "__main__":
    original_df = pd.read_csv(DATA_PATH)
    original_df = convert_types(original_df)
    original_df = process_missing_values(original_df)
    original_df['season'] = original_df['date'].apply(get_season)
    original_df = original_df.sort_values(by="date")
    original_df = one_hot_encoding(original_df, ONE_HOT_ENCODING_COLUMN_NAMES)
    original_df = original_df.drop(columns=['date'])
    # scaler = scale_data(df, NORMALIZATION_COLUMN_NAMES)
    columns_to_predict = [' pm25', ' no2', ' co']
    for column in columns_to_predict:
        df = original_df.copy()

        trainScoreRMSE_mean = []
        testScoreRMSE_mean = []
        trainScoreMAE_mean = []
        testScoreMAE_mean = []
        num_of_series = 1

        results = original_df.loc[:, original_df.columns == column]
        column_names = df.columns
        df = df.values
        scaler = MinMaxScaler()
        result_scaler = MinMaxScaler()
        result_scaler.fit(results)
        df = scaler.fit_transform(df)
        df = pd.DataFrame(df, columns=column_names)
        for i in range(num_of_series):

            train, test = train_test_split(df, .8)

            window_len = 5
            X_train, y_train = generate_sequences(train, seq_length=window_len, column=column)
            X_test, y_test = generate_sequences(test, seq_length=window_len, column=column)
            model = lstm_model(X_train)
            fit_model(model, X_train, y_train, epochs=100, batch_size=32, val_split=0.05)

            # model = load_model('lstm_model.h5')

            # make predictions
            trainPredict = model.predict([[X_train]])
            testPredict = model.predict([[X_test]])

            # transform predicted column
            # trainPredict = result_scaler.inverse_transform(trainPredict)
            # y_train = result_scaler.inverse_transform(y_train)
            testPredict = result_scaler.inverse_transform(testPredict)
            y_test = result_scaler.inverse_transform(y_test)

            # Compute Root Mean Squared Error
            # trainScoreRMSE = math.sqrt(mean_squared_error(y_train, trainPredict[:, 0]))
            testScoreRMSE = math.sqrt(mean_squared_error(y_test, testPredict[:, 0]))
            # calculate root mean absolute error
            # trainScoreMAE = math.sqrt(mean_absolute_error(y_train, trainPredict[:, 0]))
            testScoreMAE = math.sqrt(mean_absolute_error(y_test, testPredict[:, 0]))

            # trainScoreRMSE_mean.append(trainScoreRMSE)
            testScoreRMSE_mean.append(testScoreRMSE)
            # trainScoreMAE_mean.append(trainScoreMAE)
            testScoreMAE_mean.append(testScoreMAE)

        # trainScoreRMSE = meaner(trainScoreRMSE_mean)
        testScoreRMSE = meaner(testScoreRMSE_mean)
        # trainScoreMAE = meaner(trainScoreMAE_mean)
        testScoreMAE = meaner(testScoreMAE_mean)
        # print(f'{column}Train Score: %.2f RMSE' % (trainScoreRMSE))
        print(f'{column} Test Score: %.2f RMSE' % (testScoreRMSE))
        # print(f'{column}Train Score: %.2f MAE' % (trainScoreMAE))
        print(f'{column} Test Score: %.2f MAE' % (testScoreMAE))


        trainPredictPlot = np.empty_like(results)
        trainPredictPlot[:, :] = np.nan
        trainPredictPlot[window_len:len(trainPredict) + window_len, :] = trainPredict
        testPredictPlot = numpy.empty_like(df)
        testPredictPlot[:, :] = numpy.nan
        testPredictPlot[len(trainPredict) + (window_len * 2):len(df), :] = testPredict

        # plot baseline and predictions
        plt.title(f"{column} baseline and predictions")
        plt.plot(results.values)
        plt.plot(testPredictPlot)
        plt.show()
