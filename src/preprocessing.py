from datetime import date, datetime
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from settings import *


def convert_types(df):
    df = df.replace(r'^\s*$', np.nan, regex=True)  # replace empty strings with NaN
    df['date'] = pd.to_datetime(df['date'])
    df[' pm25'] = pd.to_numeric(df[' pm25'])
    df[' no2'] = pd.to_numeric(df[' no2'])
    df[' co'] = pd.to_numeric(df[' co'])
    return df


def process_missing_values(df):
    for col_name in df.columns:
        if df[col_name].dtype != 'datetime64[ns]':
            df[col_name] = df[col_name].fillna(int(df[col_name].mean()))
    return df


def one_hot_encoding(df, col_names):
    df = pd.get_dummies(df, columns=col_names, dtype=float, sparse=True)
    return df


def scale_data(df, col_names):
    min_max_scaler = MinMaxScaler()
    df[[f"{column}" for column in col_names]] = min_max_scaler.fit_transform(df[[f"{column}" for column in col_names]])
    return min_max_scaler

# source: https://randyperez.tech/blog/seasons-column
def get_season(date_time):
    # dummy leap year to include leap days(year-02-29) in our range
    leap_year = 2000
    seasons = [('winter', (date(leap_year, 1, 1), date(leap_year, 3, 20))),
               ('spring', (date(leap_year, 3, 21), date(leap_year, 6, 20))),
               ('summer', (date(leap_year, 6, 21), date(leap_year, 9, 22))),
               ('autumn', (date(leap_year, 9, 23), date(leap_year, 12, 20))),
               ('winter', (date(leap_year, 12, 21), date(leap_year, 12, 31)))]

    if isinstance(date_time, datetime):
        date_time = date_time.date()
    # we don't really care about the actual year so replace it with our dummy leap_year
    date_time = date_time.replace(year=leap_year)
    # return season our date falls in.
    return next(season for season, (start, end) in seasons
                if start <= date_time <= end)


if __name__ == "__main__":
    df = pd.read_csv(DATA_PATH)
    df = convert_types(df)
    df = process_missing_values(df)
    df['season'] = df['date'].apply(get_season)
    df = df.sort_values(by="date")
    print(df)
    df = one_hot_encoding(df, ONE_HOT_ENCODING_COLUMN_NAMES)
    scale_data(df, NORMALIZATION_COLUMN_NAMES)
    df.to_csv(PROCESSED_DATA_PATH, index=False)
