import math
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.neural_network import MLPRegressor
from preprocessing import *
from prediction import train_test_split, meaner
import numpy
from matplotlib import pyplot as plt


def generate_sequences(df, seq_length, column):
    predict_values = df.loc[:, df.columns == column].values
    df_values = df.values
    X, y = list(), list()
    for i in range(len(df_values)):
        end_ix = i + seq_length
        if end_ix > len(df_values) - 1:
            break
        # gather input and output parts of the pattern
        seq_x, seq_y = df_values[end_ix-seq_length, :-1], predict_values[end_ix]
        X.append(seq_x)
        y.append(seq_y)
    return np.array(X), np.array(y)


if __name__ == "__main__":
    original_df = pd.read_csv(DATA_PATH)
    original_df = convert_types(original_df)
    original_df = process_missing_values(original_df)
    original_df['season'] = original_df['date'].apply(get_season)
    original_df = original_df.sort_values(by="date")
    original_df = one_hot_encoding(original_df, ONE_HOT_ENCODING_COLUMN_NAMES)
    original_df = original_df.drop(columns=['date'])
    columns_to_predict = [' pm25', ' no2', ' co']

    for column in columns_to_predict:
        df = original_df.copy()

        trainScoreRMSE_mean = []
        testScoreRMSE_mean = []
        trainScoreMAE_mean = []
        testScoreMAE_mean = []
        num_of_series = 5

        results = original_df.loc[:, original_df.columns == column]
        column_names = df.columns
        df = df.values
        scaler = MinMaxScaler()
        result_scaler = MinMaxScaler()
        result_scaler.fit(results)
        df = scaler.fit_transform(df)
        df = pd.DataFrame(df, columns=column_names)
        for i in range(num_of_series):

            train, test = train_test_split(df, .8)

            model = MLPRegressor(random_state=1, max_iter=500)

            X_train, y_train = generate_sequences(train, seq_length=1, column=column)
            X_test, y_test = generate_sequences(test, seq_length=1, column=column)

            model.fit(X_train, y_train)


            # model = load_model('lstm_model.h5')

            # make predictions
            testPredict = model.predict(X_test)
            trainPredict = model.predict(X_train)

            # transform predicted column
            testPredict = result_scaler.inverse_transform(testPredict.reshape(-1, 1))
            y_test = result_scaler.inverse_transform(y_test.reshape(-1, 1))

            # Compute Root Mean Squared Error
            testScoreRMSE = math.sqrt(mean_squared_error(y_test, testPredict))
            testScoreMAE = math.sqrt(mean_absolute_error(y_test, testPredict))

            testScoreRMSE_mean.append(testScoreRMSE)
            testScoreMAE_mean.append(testScoreMAE)
            trainPredictPlot = np.empty_like(results)
            trainPredictPlot[:, :] = np.nan
            trainPredictPlot = trainPredict
            testPredictPlot = numpy.empty_like(df)
            testPredictPlot[:, :] = numpy.nan
            testPredictPlot[len(trainPredict) + (1 * 2):len(df), :] = testPredict

            # plot baseline and predictions
            plt.title(f"{column} baseline and predictions")
            plt.plot(results.values)
            plt.plot(testPredictPlot)
            plt.show()

        testScoreRMSE = meaner(testScoreRMSE_mean)
        testScoreMAE = meaner(testScoreMAE_mean)

        print(f'{column} Test Score: %.2f RMSE' % (testScoreRMSE))
        print(f'{column} Test Score: %.2f MAE' % (testScoreMAE))

