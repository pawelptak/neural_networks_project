DATA_PATH = 'raw_data/wrocław-wisniowa.csv'
DATA_TYPES = ['datetime', 'int', 'int', 'int']
LABEL_ENCODING_COLUMN_NAMES = []
ONE_HOT_ENCODING_COLUMN_NAMES = ['season']
NORMALIZATION_COLUMN_NAMES = [' pm25', ' no2', ' co']
PROCESSED_DATA_PATH = 'processed_data/processed.csv'
