from preprocessing import *
import numpy as np
import numpy
import matplotlib.pyplot as plt
from keras.models import Sequential
from sklearn.metrics import mean_squared_error
from keras.callbacks import EarlyStopping
from keras.layers import LSTM, Dense
from keras.models import load_model
import math
from sklearn.preprocessing import MinMaxScaler

def generate_sequences(df, seq_length):
    # df_values = df.values
    df_values = df
    X, y = list(), list()
    for i in range(len(df_values)):
        end_ix = i + seq_length
        if end_ix > len(df_values)-1:
            break
        # gather input and output parts of the pattern
        seq_x, seq_y = df_values[i:end_ix], df_values[end_ix]
        X.append(seq_x)
        y.append(seq_y)
    return np.array(X), np.array(y)


def train_test_split(df, train_percentage: float):
    train_size = int(len(df) * train_percentage)
    train, test = df[:train_size], df[train_size:]
    print(f"Training data: {len(train)}, test data {len(test)}")
    return train, test


def lstm_model(x):
    num_sequences = x.shape[1]
    num_features = x.shape[2]
    model = Sequential()
    model.add(LSTM(4, input_shape=(num_sequences, num_features)))
    model.add(Dense(num_features))
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model


def fit_model(model, X_train, y_train, epochs, batch_size, val_split,  plot=True):
    # early stopping doesnt work for this model for some reason
    callback = EarlyStopping(monitor='loss', patience=3)
    history = model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size,
                        validation_split=val_split, shuffle=False, callbacks=[callback]).history

    if plot:
        # plot the training losses
        fig, ax = plt.subplots(figsize=(14, 6), dpi=80)
        ax.plot(history['loss'], 'b', label='Train', linewidth=2)
        ax.plot(history['val_loss'], 'r', label='Validation', linewidth=2)
        ax.set_title('Model loss', fontsize=16)
        ax.set_ylabel('Loss (mae)')
        ax.set_xlabel('Epoch')
        ax.legend(loc='upper right')
        plt.show()

    model.save('lstm_model.h5')


if __name__ == "__main__":
    original_df = pd.read_csv(DATA_PATH)
    original_df = convert_types(original_df)
    original_df = process_missing_values(original_df)
    original_df['season'] = original_df['date'].apply(get_season)
    original_df = original_df.sort_values(by="date")
    original_df = one_hot_encoding(original_df, ONE_HOT_ENCODING_COLUMN_NAMES)

    columns_to_predict = [' pm25', ' no2', ' co']
    for column in columns_to_predict:
        df = original_df.loc[:, original_df.columns == column]

        df = df.values
        scaler = MinMaxScaler()
        df = scaler.fit_transform(df)

        train, test = train_test_split(df, .8)

        window_len = 5
        X_train, y_train = generate_sequences(train, seq_length=window_len)
        X_test, y_test = generate_sequences(test, seq_length=window_len)
        model = lstm_model(X_train)
        fit_model(model, X_train, y_train, epochs=100, batch_size=32, val_split=0.05)

        # model = load_model('lstm_model.h5')

        # make predictions
        trainPredict = model.predict(X_train)
        testPredict = model.predict(X_test)

        # invert predictions
        trainPredict = scaler.inverse_transform(trainPredict)
        y_train = scaler.inverse_transform(y_train)
        testPredict = scaler.inverse_transform(testPredict)
        y_test = scaler.inverse_transform(y_test)

        # calculate root mean squared error
        trainScore = math.sqrt(mean_squared_error(y_train[:], trainPredict[:]))
        print('Train Score: %.2f RMSE' % (trainScore))
        testScore = math.sqrt(mean_squared_error(y_test[:], testPredict[:]))
        print('Test Score: %.2f RMSE' % (testScore))

        # shift train predictions for plotting
        trainPredictPlot = numpy.empty_like(df)
        trainPredictPlot[:, :] = numpy.nan
        trainPredictPlot[window_len:len(trainPredict)+window_len, :] = trainPredict
        # shift test predictions for plotting
        testPredictPlot = numpy.empty_like(df)
        testPredictPlot[:, :] = numpy.nan
        testPredictPlot[len(trainPredict)+(window_len*2):len(df), :] = testPredict

        # plot baseline and predictions
        plt.plot(scaler.inverse_transform(df))
        plt.plot(testPredictPlot)
        plt.show()