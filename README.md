# Project - Predicting pollutants with LSTM neural network
Dataset source: [aqicn.org](https://aqicn.org/) (Wroclaw - Wisniowa 10.06.2015 - 01.10.2021, Beijing 31.12.2013 - 02.01.2022)


### Tensorflow GPU support requirements: 
```sh
CUDA 
cuDNN
```
\
cuDNN installation: 
```sh
Copy files from [installpath]\cuda\bin\ to C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v[version]\bin.
Copy files from [installpath]\cuda\include\ to C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v[version]\include.
Copy files from [installpath]\cuda\lib\ to C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v[version]\lib.
```
